﻿using Microsoft.AspNetCore.Identity;

namespace AspNetCore.Identity.WebApp.Stores
{
    public sealed class ApplicationUser : IdentityUser
    {
    }
}