﻿namespace Microsoft.Extensions.Identity.Tests.Stores
{
    public sealed class ApplicationUser
    {
        public ApplicationUser(string id, string userName)
        {
            Id = id;
            UserName = userName;
        }

        public string Id { get; }
        public string UserName { get; }
    }
}