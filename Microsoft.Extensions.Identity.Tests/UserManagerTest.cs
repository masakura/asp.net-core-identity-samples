﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Identity.Tests.Stores;
using Xunit;

namespace Microsoft.Extensions.Identity.Tests
{
    public sealed class UserManagerTest
    {
        public UserManagerTest()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var services = serviceCollection.BuildServiceProvider();

            _userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
        }

        private readonly UserManager<ApplicationUser> _userManager;

        private static void ConfigureServices(ServiceCollection services)
        {
            services.AddIdentityCore<ApplicationUser>()
                .AddUserStore<UserStore>();
        }

        [Fact]
        public async Task TestFindByIdAsync()
        {
            var user = await _userManager.FindByIdAsync("user1");

            Assert.Equal("taro", user.UserName);
        }
    }
}