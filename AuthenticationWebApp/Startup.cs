﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace AuthenticationWebApp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddCookie();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseAuthentication();

            app.Map("/challenge", builder => builder.Run(context =>
            {
                // ReSharper disable once ConvertToLambdaExpression
                return context.ChallengeAsync();
            }));

            app.Map("/sign-in", builder => builder.Run(context =>
            {
                var identity = new ClaimsIdentity();
                identity.AddClaim(new Claim(ClaimTypes.Name, "taro"));
                var user = new ClaimsPrincipal(identity);

                return context.SignInAsync(user);
            }));

            app.Map("/user", builder => builder.Run(context =>
            {
                var user = context.User.FindFirstValue(ClaimTypes.Name);

                return context.Response.WriteAsync(user);
            }));

            app.Run(async context => { await context.Response.WriteAsync("Hello World!"); });
        }
    }
}